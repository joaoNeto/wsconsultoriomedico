var query = document.location
                   .toString()
                   .replace(/^.*?\?/, '')
                   .replace(/#.*$/, '')
                   .split('&');
var aux = decodeURIComponent(query[0]).split('=');
let idConsulta = aux[1];

if(sessionStorage.token == "" || typeof sessionStorage.token == "undefined"){
	window.location =  "../login/index.html";
}

$(document).ready(function(){
	$("#exitApp").click(function(){
		delete sessionStorage.token;
		delete sessionStorage.tipo_pessoa;
		window.location = "../login/index.html";
	});

	$(".salvarAvaliacao").click(function(){
		$(this).html("salvando...");
		$(this).prop("disabled", true);
		var form = {};
		$(".dadosForm").serializeArray().forEach(function(line){
			eval("form."+line.name+" = '"+line.value+"';");
		});
		form.id_consulta = idConsulta;

    $.ajax({
        type: "POST",
        url: url+"avaliacao",
        data: form,
        beforeSend: function(xhr){xhr.setRequestHeader('token', sessionStorage.token);},
        success: function( response ){

        }
    });


	});

});