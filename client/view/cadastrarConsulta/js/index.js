if(sessionStorage.token == "" || typeof sessionStorage.token == "undefined"){
	window.location =  "../login/index.html";
}

$(document).ready(function(){
	listarMedico();
	listarPaciente();

	$("#exitApp").click(function(){
		delete sessionStorage.token;
		delete sessionStorage.tipo_pessoa;
		window.location = "../login/index.html";
	});

	$('.mask.data').mask('00/00/0000');

	$(".salvar").click(function(){
		$(this).prop('disabled', true);
		$(this).html("salvando...");

		var form = {};
		$("form.consulta").serializeArray().forEach(function(line){
			eval("form."+line.name+" = '"+line.value+"';");
		});

	    $.ajax({
	        type: "POST",
	        url: url+"consulta",
	        data: form,
	        beforeSend: function(xhr){xhr.setRequestHeader('token', sessionStorage.token);},
	        success: function( response ){
				$(".salvar").prop('disabled', false);
				$(".salvar").html("Salvar");
				$.notify({ message: response.msg },{type: 'success'});
				setTimeout(function(){ window.location = "../dashboardAtendente/index.html"; }, 1000);
	        },
	        error: function ( server ){
				$(".salvar").prop('disabled', false);
				$(".salvar").html("Salvar");
				$.notify({ message: server.responseJSON.msg },{type: 'danger'});	        	
	        }
		});

	});

});

function listarMedico(){
    $.ajax({
        type: "GET",
        url: url+"pessoa?id_tipo_pessoa=2",
        beforeSend: function(xhr){xhr.setRequestHeader('token', sessionStorage.token);},
        success: function( response ){
        	var htmlElement = "";
        	response.data.forEach(function(line){
        		htmlElement += "<option value='"+line.crm+"'> "+line.crm+" - "+line.nome_completo+" </option>";
        	});
 			$("select.medico").html(htmlElement);
        }
	});
}

function listarPaciente(){
    $.ajax({
        type: "GET",
        url: url+"pessoa?id_tipo_pessoa=3",
        beforeSend: function(xhr){xhr.setRequestHeader('token', sessionStorage.token);},
        success: function( response ){
        	var htmlElement = "";
        	response.data.forEach(function(line){
        		htmlElement += "<option value='"+line.cpf_paciente+"'> "+line.cpf_paciente+" - "+line.nome_completo+" </option>";
        	});
 			$("select.paciente").html(htmlElement);
        }
	});
}
