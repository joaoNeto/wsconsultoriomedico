if(sessionStorage.token == "" || typeof sessionStorage.token == "undefined"){
	window.location =  "../login/index.html";
}

$(document).ready(function(){

	$('.mask.data').mask('00/00/0000');
	$('.mask.cpf').mask('000.000.000-00');
	$('.mask.tel').mask('(00) 00000 - 0000');


	$("#exitApp").click(function(){
		delete sessionStorage.token;
		delete sessionStorage.tipo_pessoa;
		window.location = "../login/index.html";
	});

	$(".salvar").click(function(){
		$(this).prop('disabled', true);
		$(this).html("salvando...");
		var form = {};
		$(".formPaciente").serializeArray().forEach(function(line){
			eval("form."+line.name+" = '"+line.value+"';");
		});
		
		form.id_tipo_pessoa = 3;
		form.login = new Date().getTime();
		form.senha = MD5(new Date().getTime());

	    $.ajax({
	        type: "POST",
	        url: url+"pessoa",
	        data: form,
	        dataType: "json",
	        beforeSend: function(xhr){xhr.setRequestHeader('token', sessionStorage.token);},
	        success: function( response ){
				$(".salvar").prop('disabled', false);
				$(".salvar").html("Salvar");
				$.notify({ message: response.msg },{type: 'success'});
				setTimeout(function(){ window.location = "../dashboardAtendente/index.html"; }, 1000);
	        },
	        error: function(server){
				$(".salvar").prop('disabled', false);
				$(".salvar").html("Salvar");
				$.notify({ message: server.responseJSON.msg },{type: 'danger'});
	        }
		});


	});
});