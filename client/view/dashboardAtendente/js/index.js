if(sessionStorage.token == "" || typeof sessionStorage.token == "undefined"){
	window.location =  "../login/index.html";
}

$(document).ready(function(){
    listarCliente();
    listarConsultas();
	$("#exitApp").click(function(){
		delete sessionStorage.token;
		delete sessionStorage.tipo_pessoa;
		window.location = "../login/index.html";
	});

	$(".filtrarPaciente").click(function(){
		listarCliente($(".nomePaciente").val());
	});

    $(".filtrarConsulta").click(function(){
        listarConsultas($(".filtroData").val(), $(".filtroNome").val());
    });

});

function listarConsultas(filtroData = "", filtroNome = ""){
    var where = "";

    if(typeof filtroData != '')
        where += "&data="+filtroData;

    if(typeof filtroNome != '')
        where += "&search="+filtroNome;

    $.ajax({
        type: "GET",
        url: url+"consulta?v=v"+where,
        beforeSend: function(xhr){xhr.setRequestHeader('token', sessionStorage.token);},
        success: function( response ){
            var htmlElement = "";
            response.data.forEach(function(line){
                htmlElement += "<tr>";
                htmlElement += "<td>"+line.data_consulta_br+"</td>";
                htmlElement += "<td>"+line.nome_paciente+"</td>";
                htmlElement += "<td>"+line.email_paciente+"</td>";
                htmlElement += "<td>"+line.telefone_paciente+"</td>";
                htmlElement += "<td>"+line.nome_medico+"</td>";
                htmlElement += "<td><i id_consulta='"+line.id_consulta+"' class='fa fa-trash btn btn-danger btn-sm excluirConsulta' aria-hidden='true' ></i></td>";
                htmlElement += "</tr>";
            });

            if(htmlElement == ""){
                $("tbody.consulta").html("<tr><td colspan='5'> Sem dados.</td></tr>");
            }else{
                $("tbody.consulta").html(htmlElement);
            }

            $(".excluirConsulta").click(function(){
                swal("Deseja realmente excluir a consulta?").then((value) => {
                  if(value){
                    $.ajax({
                        type: "DELETE",
                        url: url+"consulta?id_consulta="+$(this).attr('id_consulta'),
                        beforeSend: function(xhr){xhr.setRequestHeader('token', sessionStorage.token);},
                        success: function( response ){
                            listarConsultas();
                            $(".filtroData").val(""); 
                            $(".filtroNome").val("");
                            $.notify({ message: response.msg },{type: 'success'});
                        },
                        error: function( server ){
                            $.notify({ message: server.responseJSON.msg },{type: 'danger'});
                        }
                    });
                  }
                });
            });
        }
    });
}

function listarCliente(filtro = ""){
	var where  = "";
	if(typeof filtro != '')
		where += "&search="+filtro;

    $.ajax({
        type: "GET",
        url: url+"pessoa?id_tipo_pessoa=3"+where,
        beforeSend: function(xhr){xhr.setRequestHeader('token', sessionStorage.token);},
        success: function( response ){
        	var htmlElement = "";
        	response.data.forEach(function(line){
        		htmlElement += "<tr>";
        		htmlElement += "<td>"+line.nome_completo+"</td>";
        		htmlElement += "<td>"+line.email+"</td>";
        		htmlElement += "<td>"+line.sexo+"</td>";
        		htmlElement += "<td>"+line.telefone1+"</td>";
                htmlElement += "<td>"+line.telefone2+"</td>";
                htmlElement += "<td><i id_pessoa='"+line.id_pessoa+"' class='fa fa-trash btn btn-danger btn-sm excluirPaciente' aria-hidden='true' ></i></td>";
        		htmlElement += "</tr>";
        	});

            if(htmlElement == ""){
                $("tbody.paciente").html("<tr><td colspan='5'> Sem dados.</td></tr>");
            }else{
                $("tbody.paciente").html(htmlElement);
            }


            $(".excluirPaciente").click(function(){

                swal("Deseja realmente excluir paciente?")
                .then((value) => {
                  if(value){
                    $.ajax({
                        type: "DELETE",
                        url: url+"pessoa?id_pessoa="+$(this).attr('id_pessoa'),
                        beforeSend: function(xhr){xhr.setRequestHeader('token', sessionStorage.token);},
                        success: function( response ){
                            listarCliente();
                            $(".nomePaciente").val("");
                            $.notify({ message: response.msg },{type: 'success'});  
                        },
                        error: function( server ){
                            $.notify({ message: server.responseJSON.msg },{type: 'danger'});
                        }
                    });            
                  }
                });        
                
            });

        }
	});

} 