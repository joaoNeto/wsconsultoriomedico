if(sessionStorage.token != "" || typeof sessionStorage.token != "undefined"){
	switch (sessionStorage.tipo_pessoa) {
	    case "1":
    		// redirecionar para o dashboard atendente
        	window.location =  "../dashboardAtendente/index.html";
	        break;
	    case "2":
    		// redirecionar para o dashboard medio
        	window.location = "../dashboardMedico/index.html";
	        break;
	}
}

$("#login-button").click(function(event){

	event.preventDefault();	 
	$('form').fadeOut(500);
	$('.wrapper').addClass('form-success');
	$("h1").html("carregando...");
	
    $.ajax({
        type: "POST",
        url: url+"login",
        data: {"login": $("#login").val(), "senha": MD5($("#senha").val())},
        dataType: "json",
        success: function( response ){

			$("h1").html("Bem vindo");
        	sessionStorage.token = response.token;
        	sessionStorage.tipo_pessoa = response.tipo_pessoa;
        	sessionStorage.nome_usuario = response.nome_usuario;

        	switch (sessionStorage.tipo_pessoa) {
			    case "1":
	        		// redirecionar para o dashboard atendente
		        	window.location =  "../dashboardAtendente/index.html";
			        break;
			    case "2":
	        		// redirecionar para o dashboard medio
		        	window.location = "../dashboardMedico/index.html";
			        break;
			    default: 
					$("h1").html("Voce não possui acesso ao sistema!");
			    	break;
			}

        },
		error: function(server){

			$('form').fadeIn(500);
			$('.wrapper').removeClass('form-success');
			$("h1").html("Bem vindo");

			$.notify({
				message: server.responseJSON.msg 
			},{
				type: 'danger'
			});
		}

    });

});