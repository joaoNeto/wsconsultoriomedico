<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Avaliacao extends CI_Controller {

	/*
	crm
	data
	data_inicio e data_inicio
	id_atendente
	id_paciente
	*/
	public function get(){
		$this->load->model('avaliacao');
		$arrayModel = $this->avaliacao->read($_GET);
		if ($arrayModel['status']){
			$this->responseClient(200, [
			'msg' => 'Lista de Avaliações!', 
			'data' => $arrayModel['result']
			]);
		}else{
			$this->responseClient(200,['msg' => $arrayModel['msg']]);
		}

		
	}

	public function post(){

		$this->load->model('avaliacao');
		$result = $this->avaliacao->create();

		if($result['status']){
			$this->responseClient(200, ['msg' => 'Inserção realizada com sucesso','id_consulta' => $result['id_consulta']]);

		}else{
			$this->responseClient(400, ['msg' => 'Verifique se os dados foram inseridos corretamente.']);
		}

	}

	public function put(){
		if( !isset($_GET['id_consulta']) )
			$this->responseClient(400, ['msg' => 'Por favor informar o id_consulta!']);
		$this->load->model('avaliacao');
		$atualizou = $this->avaliacao->atualizar();
		if($atualizou['status']){
			$this->responseClient(200, ['msg' => 'Alteração concluída com sucesso!']);
		}else{
			$this->responseClient(400, [
				'msg' => 'Erro ao alterar avaliacao!',
				'msgDetails' => $atualizou['msg']
			]);
		}
	}
	public function delete(){
		if( !isset($_GET['id_consulta']))
			$this->responseClient(400, ['msg' => 'Parâmetro não inicializado!']);
	
	    $this->load->model('consulta');
	    $this->load->model('avaliacao');

	    $deletou = $this->consulta->delete();
	    $deletou = $this->avaliacao->delete();

	    if($deletou['status']){
			$this->responseClient(200, ['msg' => 'Exclusão concluída com sucesso!']);
		}else{
			$this->responseClient(400, [
				'msg' => 'Erro na exclusão de dados!',
				'msgDetails' => $delete['msg']
			]);

		}
	}

}
