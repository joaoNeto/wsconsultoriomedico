<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consulta extends CI_Controller {

	/*
	id_paciente
	id_atendente
	data_inicio e data_fim
	*/
	public function get(){
		
		try{
			$this->load->model('consulta');
			$array = $this->consulta->read($_GET);
		
			if($array['status']){
				$this->responseClient(200, [
					'msg' => 'Lista de consultas!',
					'data' => $array['result']
				]);
			}else{
				$this->responseClient(400, [
					'msg' => $array['msg']
				]);
			}
		}catch(Exception $ex){
			$this->responseClient(400, ['msg' => 'Erro no sistema']);
		}

	}

	public function post(){
		$this->load->model('consulta');
		
		if(!isset($_POST['data']) && !isset($_POST['cpf_atendente']) && !isset($_POST['crm']))
			$this->responseClient(400, ['msg' => 'Passe os parametros corretamente!']);
		$result = $this->consulta->create();
		if($result['status'])	
			$this->responseClient(200, ['msg' => 'Consulta concluída com sucesso!', 'id_consulta' => $result['id_consulta']]);
		else
			$this->responseClient(400, [
				'msg' => 'Erro ao inserir consulta!',
				'msgDetails' => $result['msgDetails']]);
	}

	public function put(){
	
		if( !isset($_GET['id_consulta']) )
			$this->responseClient(400, ['msg' => 'Por favor informar o id_consulta!']);

		$this->load->model('consulta');

		$atualizou = $this->consulta->update();

		if($atualizou['status']){
			$this->responseClient(200, ['msg' => 'Consulta concluída com sucesso!']);
		}else{
			$this->responseClient(400, [
				'msg' => 'Erro ao alterar consulta!',
				'msgDetails' => $atualizou['msg']
			]);
		}

	}

	public function delete(){
		if( !isset($_GET['id_consulta']) &&
			!isset($_GET['cpf_paciente']) &&
			!isset($_GET['cpf_atendimento']) && 
			!isset($_GET['crm'])
			 )
			$this->responseClient(400, ['msg' => 'Parâmetro não inicializado!']);
	
	    $this->load->model('consulta');

	    $deletou = $this->consulta->delete();

	    if($deletou['status']){
			$this->responseClient(200, ['msg' => 'Exclusão concluída com sucesso!']);
		}else{
			$this->responseClient(400, [
				'msg' => 'Erro na exclusão de dados!',
				'msgDetails' => $delete['msg']
			]);

		}
	}

}