<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function get(){
			$this->responseClient(400, ['msg' => 'uelcomi']);		
	}

	public function post(){

		$search = [
			'login' => $_POST['login'],
			'senha' => $_POST['senha'], // em md5
			'not_show_paciente' => true
		];

		$this->load->model('pessoa');

		$pessoa = $this->pessoa->read($search);
		if($pessoa->result_id->num_rows > 0)
			$this->responseClient(200, [
				'msg' 		   => 'Logado com sucesso!', 
				'token' 	   => $this->encriptData($pessoa->result()[0]),
				'tipo_pessoa'  => $pessoa->result()[0]->fk_id_tipo_pessoa,
				'nome_usuario' => $pessoa->result()[0]->nome_completo
			]);
		else
			$this->responseClient(400, ['msg' => 'Usuário não encontrado!']);
	}

}
