<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pessoa extends CI_Controller {

	/*
	@apiParam id_tipo_pessoa
	@apiParam id_pessoa
	*/
	public function get(){
		$this->load->model('pessoa');

		$this->responseClient(200, [
			'msg' => 'Lista de pessoas!', 
			'data' => $this->pessoa->read($_GET)->result_array()
		]);
	}

	/*
	@apiParam id_pessoa
	@apiParam login
	@apiParam senha
	@apiParam nome_comleto
	@apiParam email
	@apiParam sexo
	@apiParam estado
	@apiParam dt_nascimento
	@apiParam estado_civil
	@apiParam telefone1
	@apiParam telefone2
	@apiParam id_tipo_pessoa
	@apiParam cpf parametro passado caso a pessoa seja atendente
	@apiParam crm  parametro passado caso a pessoa seja medico
	@apiParam id_especializacao parametro passado caso a pessoa seja medico
	@apiParam cpf parametro passado caso a pessoa seja paciente
	@apiParam rg parametro passado caso a pessoa seja paciente
	@apiParam tipo_sanguineo parametro passado caso a pessoa seja paciente
	*/
	public function post(){
		$this->load->model('pessoa');

		// VERIFICAR SE ESSA PESSOA JÁ EXISTE
		$pessoaExiste = $this->pessoa->read(['login' => $_POST['login']]);
		if($pessoaExiste->result_id->num_rows > 0)
			$this->responseClient(400, ['msg' => 'Este login já está cadastrado no sistema!']);
        
        //VALIDANDO CPF
        if($_POST['id_tipo_pessoa'] != '2')
			if(!$this->validaCPF($_POST['cpf']))
				$this->responseClient(402, ['msg' => 'CPF inválido!']);

		// CADASTRAR PESSOA
		$cadastrou = $this->pessoa->create();
		if($cadastrou){

			$pessoa = $this->pessoa->read(['login' => $_POST['login']]);
			$id_pessoa = $pessoa->result()[0]->id_pessoa;

			switch ($_POST['id_tipo_pessoa']) {
				case '1':
					// cadastrando o atendente
					$this->load->model('atendente');
					$this->atendente->create($id_pessoa);
					break;
				case '2':
					// cadastrando o médico
					$this->load->model('medico');
					$this->medico->create($id_pessoa);
					break;
				case '3':
					// cadastrando o paciente
					$this->load->model('paciente');
					$this->paciente->create($id_pessoa);
					break;
			}

			$this->responseClient(200, ['msg' => 'Cadastrado com sucesso!', 'id_pessoa' => $id_pessoa]);
		}
		else{
			$this->responseClient(400, ['msg' => 'Erro ao cadastrar pessoa!']);
		}
	}


	/*

	*/
	public function put(){

		if( !isset($_GET['id_pessoa']) )
			$this->responseClient(400, ['msg' => 'Por favor informar o id_pessoa!']);

		$this->load->model('pessoa');

		// VALIDANDO SE PESSOA EXISTE

		$arr = $this->pessoa->read(['id_pessoa' => $_GET['id_pessoa']])->result_array();

		if(count($arr) == 0)
			$this->responseClient(400, ['msg' => 'Pessoa que voce deseja alterar não existe!']);


		// ALTERANDO PESSOA
		$atualizou = $this->pessoa->update();

		if($atualizou['status']){
			$this->responseClient(200, ['msg' => 'Pessoa alterada com sucesso!']);
		}else{
			$this->responseClient(400, [	
				'msg' => 'Erro ao alterar pessoa!',
				'msgDetails' => $atualizou['msg']
			]);
		}

	}


	public function delete(){

		if(!isset($_GET['id_pessoa']))
			$this->responseClient(400, ['msg' => 'Por favor informar a pessoa que será deletada']);
	
	    $this->load->model('pessoa');

	    // VALIDANDO SE PESSOA EXISTE
		$arr = $this->pessoa->read(['id_pessoa' => $_GET['id_pessoa']])->result_array();
		if(count($arr) == 0)
			$this->responseClient(400, ['msg' => 'A pessoa que voce deseja excluir não existe!']);

	    $deletou = $this->pessoa->delete();
	    if($deletou['status']){
			$this->responseClient(200, ['msg' => 'Exclusão concluída com sucesso!']);
		}else{
			$this->responseClient(400, [
				'msg' => 'Erro na exclusão de dados!',
				'msgDetails' => $delete['msg']
			]);

		}
	}
	

	/* ---- funções utils
	------------------------------------------------------*/

	function validaCPF($cpf = null) {

	// Verifica se um número foi informado
	if(empty($cpf)) {
		return false;
	}

	// Elimina possivel mascara
	$cpf = preg_replace("/[^0-9]/", "", $cpf);
	$cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
	
	// Verifica se o numero de digitos informados é igual a 11 
	if (strlen($cpf) != 11) {
		return false;
	}
	// Verifica se nenhuma das sequências invalidas abaixo 
	// foi digitada. Caso afirmativo, retorna falso
	else if ($cpf == '00000000000' || 
		$cpf == '11111111111' || 
		$cpf == '22222222222' || 
		$cpf == '33333333333' || 
		$cpf == '44444444444' || 
		$cpf == '55555555555' || 
		$cpf == '66666666666' || 
		$cpf == '77777777777' || 
		$cpf == '88888888888' || 
		$cpf == '99999999999') {
		return false;
	 // Calcula os digitos verificadores para verificar se o
	 // CPF é válido
	 } else {   
		
		for ($t = 9; $t < 11; $t++) {
			
			for ($d = 0, $c = 0; $c < $t; $c++) {
				$d += $cpf{$c} * (($t + 1) - $c);
			}
			$d = ((10 * $d) % 11) % 10;
			if ($cpf{$c} != $d) {
				return false;
			}
		}

		return true;
	}
	}

}
