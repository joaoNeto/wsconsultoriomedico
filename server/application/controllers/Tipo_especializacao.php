<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_especializacao extends CI_Controller {


	public function get(){
		$this->load->model('tipo_especializacao');
		$this->responseClient(200, [
			'msg'  => 'Lista de tipos de especialização', 
			'data' => $this->tipo_especializacao->read()->result_array()
		]);
	}
	}
