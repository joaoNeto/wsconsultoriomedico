<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_pessoa extends CI_Controller {

	public function get(){
		$this->load->model('tipo_pessoa');
		$this->responseClient(200, [
			'msg'  => 'Lista de tipos de pessoa', 
			'data' => $this->tipo_pessoa->read()->result_array()
		]);
	}

}
