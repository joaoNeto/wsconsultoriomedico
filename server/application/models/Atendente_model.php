<?php
class Atendente_model extends CI_Model {

	public $fk_id_pessoa;
	public $cpf_atendente;

	public function create($id_pessoa){
		$this->load->database();
		try{
			$this->fk_id_pessoa = $id_pessoa;
			$this->cpf_atendente = $_POST['cpf'];
		    return $this->db->insert('tb_atendente', $this);
		}catch(Exception $ex){
			return false;
		}
	}

	public function delete(){
		 try{
			$this->load->database();
			if(isset($_GET['id_pessoa'])){
		    	$this->db->where('fk_id_pessoa',$_GET['id_pessoa']);
         	   $deletou = $this->db->delete('tb_pessoa');
				return ['status' => $deletou];
			}
			return ['status' => false, 'msg' => 'A variável id_pessoa precisa ser inicializada!'];

		}catch(Exception $ex){
			return ['status' => false, 'msg' => $ex->getMessage()];
		}
   }
}