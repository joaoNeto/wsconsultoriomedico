<?php
class Avaliacao_model extends CI_Model {

    public $sintomas;
	public $diagnostico;
	public $procedimento_imediato;
	public $procedimento_posterior;
	public $exame;
	public $fk_id_consulta;

	public function create(){
		$this->load->database();
		try{

		    // INSERINDO A AVALIACAO
			$this->sintomas 	  = isset($_POST['sintomas']) ? $_POST['sintomas'] : '';
			$this->diagnostico 	  = isset($_POST['diagnostico']) ? $_POST['diagnostico'] : '';
			$this->procedimento_imediato  = isset($_POST['procedimento_imediato']) ? $_POST['procedimento_imediato'] : '';
			$this->procedimento_posterior = isset($_POST['procedimento_posterior']) ? $_POST['procedimento_posterior'] : '';
			$this->exame 		  = isset($_POST['exame']) ? $_POST['exame'] : '';
			$this->fk_id_consulta = isset($_POST['id_consulta']) ? $_POST['id_consulta'] : '';
		    $cadastrou = $this->db->insert('tb_avaliacao', $this);


		    return [
		    	'status' => $cadastrou,
		    	'id_consulta' => $_POST['id_consulta']
			];

		}catch(Exception $ex){
		    return [
		    	'status' => false,
		    	'msg' => $ex->getMessage()
			];
		}
	}

	public function read($where = []){
		try{
			$this->load->database();
			$this->db->select('*');
			$this->db->from('tb_avaliacao');
			$this->db->join('tb_consulta', 'tb_consulta.id_consulta = tb_avaliacao.fk_id_consulta', 'inner');
			//$this->db->join('tb_paciente', 'tb_paciente.fk_id_pessoa = tb_consulta.id_paciente', 'left');
			//$this->db->join('tb_atendente', 'tb_atendente.fk_id_pessoa = tb_consulta.id_atendente', 'left');
			//$this->db->join('tb_pessoa as p_atendente', 'p_atendente.id_pessoa = tb_atendente.fk_id_pessoa', 'left');
			//$this->db->join('tb_pessoa as p_paciente', 'p_paciente.id_pessoa = tb_paciente.fk_id_pessoa', 'left');

			// filtrar por médico
			//if(isset($where['fk_crm_medico']))
				$this->db->where('');

			if(isset($where['data']))
				$this->db->where('');

			if(isset($where['data_inicio']) && isset($where['data_inicio']))
				$this->db->where('');

			//if(isset($where['id_atendente']))
				//$this->db->where('');

			//if(isset($where['id_paciente']))
				// $this->db->where('');

			$resultQuery = $this->db->get()->result();
		    return [
		    	'status' => true,
		    	'result' => $resultQuery
			];
		}catch(Exception $ex){
			return ['status' => false, 'msg' => $ex->getMessage()];
		}

	}

	public function atualizar(){
		try{
			$this->load->database();
			$arrUpdate = [];

			if(isset($_GET['fk_crm_medico']))
				$arrUpdate['fk_crm_medico'] = $_GET['fk_crm_medico'];

			if(isset($_GET['sintomas']))
				$arrUpdate['sintomas']  = $_GET['sintomas'];

			if(isset($_GET['procedimento_imediato']))
				$arrUpdate['procedimento_imediato']  = $_GET['procedimento_imediato'];

			if(isset($_GET['procedimento_posterior']))
				$arrUpdate['procedimento_posterior']  = $_GET['procedimento_posterior'];

			if(isset($_GET['exame']))
				$arrUpdate['exame']  = $_GET['exame'];

	        $result = $this->db->update('tb_avaliacao', $arrUpdate, array('fk_id_consulta' => $_GET['id_consulta']));

	        return ['status' => $result];
        }catch(Exception $ex){
        	return ['status' => false, 'msg' => $ex->getMessage()];
        }		
	}

	public function delete(){
			try{
			$this->load->database();

			if(isset($_GET['id_consulta'])){
				$this->db->where('fk_id_consulta', $_GET['id_consulta']);
				$deletou = $this->db->delete('tb_avaliacao');
			}else{
				$deletou = false;
			}

			return ['status' => $deletou];
		}catch(Exception $ex){
			return ['status' => false, 'msg' => $ex->getMessage()];
		}

	}

}
