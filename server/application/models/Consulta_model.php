	<?php
class Consulta_model extends CI_Model {

	public $id_consulta;
	public $data;
	public $cpf_paciente;
	public $cpf_atendente;
	public $crm;

	public function create(){
		try{

			$this->load->database();
			$this->data 		 = date("Y-m-d", strtotime($_POST['data']));
			$this->cpf_paciente  = $_POST['cpf_paciente'];
			$this->cpf_atendente = $this->sessionUser->cpf_paciente == null ? $this->sessionUser->cpf_atendente : $this->sessionUser->cpf_paciente;
			$this->crm = $_POST['crm'];
		    $inseriu = $this->db->insert('tb_consulta', $this);

		    $id_consulta = null;
		  //   if($inseriu){
				// // PEGANDOO O ID
				// $this->db->select('id_consulta');
				// $this->db->from('tb_consulta');
				// $this->db->where('data = ', date("Y-m-d", strtotime($_POST['data'])));
				// $this->db->where('cpf_atendente = ',$_POST['cpf_paciente']);
				// $this->db->where('crm = ', $_POST['crm']);
				// $this->db->where('cpf_paciente = ', $_POST['cpf_paciente']);
				
				// $id_consulta = $this->db->get()->result()[0]->id_consulta;
		  //   }

			return [
				'status' => $inseriu,
				'id_consulta' => $id_consulta
			];
		}catch(Exception $ex){
			return [
				'status' => false,
				'msgDetails' => $ex->getMessage()
			];
		}
	}

	public function read($where = []){
		try{
			$this->load->database();
			$this->db->select('c.*, pessoa_paciente.nome_completo as nome_paciente, pessoa_paciente.email as email_paciente, pessoa_paciente.telefone1 as telefone_paciente, pessoa_medico.nome_completo as nome_medico,  DATE_FORMAT(c.data, "%d/%m/%Y %h:%i:%s") data_consulta_br ');
			$this->db->from('tb_consulta as c');
			$this->db->join('tb_paciente paci', 'paci.cpf_paciente = c.cpf_paciente', 'left');
			$this->db->join('tb_pessoa pessoa_paciente', 'paci.fk_id_pessoa = pessoa_paciente.id_pessoa', 'left');			
			$this->db->join('tb_atendente atend', 'atend.cpf_atendente = c.cpf_atendente', 'left');
			$this->db->join('tb_medico as med', 'med.crm = c.crm', 'left');
			$this->db->join('tb_pessoa pessoa_medico', 'med.fk_id_pessoa = pessoa_medico.id_pessoa', 'left');

			if($this->sessionUser->fk_id_tipo_pessoa == '2'){
				$this->db->where(' c.crm = '.$this->sessionUser->crm);
			}
		    
			// filtrar por médico
			if(isset($where['search']))
				$this->db->where('pessoa_medico.nome_completo like "%'.$where['search'].'%" OR pessoa_paciente.nome_completo like "%'.$where['search'].'%"');

			$resultQuery = $this->db->get()->result();
		    return [
		    	'status' => true,
		    	'result' => $resultQuery
			];
		}catch(Exception $ex){
			return ['status' => false, 'msg' => $ex->getMessage()];
		}
	}

	public function update(){
        try{
			$this->load->database();

			if(isset($_POST['cpf_paciente']))
				$this->id_paciente = $_POST['cpf_paciente'];

			if(isset($_POST['cpf_atendente']))
				$this->data = $_POST['cpf_atendente'];
			
			if(isset($_POST['crm']))
				$this->data = $_POST['crm'];

	        $result = $this->db->update('tb_consulta', $this, array('id_consulta' => $_POST['id_consulta']));

	        return ['status' => $result];
        }catch(Exception $ex){
        	return ['status' => false, 'msg' => $ex->getMessage()];
        }		
	}

	public function delete(){
		try{
			$this->load->database();

			if(isset($_GET['id_consulta']))
				$this->db->where('id_consulta', $_GET['id_consulta']);
			
			$deletou = $this->db->delete('tb_consulta');

			return ['status' => $deletou];
		}catch(Exception $ex){
			return ['status' => false, 'msg' => $ex->getMessage()];
		}

	}
}