<?php
class Medico_model extends CI_Model {

	public $fk_id_pessoa;
	public $crm;
	public $fk_id_medico_especializacao;

	public function create($id_pessoa){
		$this->load->database();
		try{
			$this->fk_id_pessoa = $id_pessoa;
			$this->crm = isset($_POST['crm']) ? $_POST['crm'] : '';
			$this->fk_id_medico_especializacao = isset($_POST['id_especializacao']) ? $_POST['id_especializacao'] : '';
		    return $this->db->insert('tb_medico', $this);
		}catch(Exception $ex){
			return false;
		}
	}

		public function read($where){
		$this->load->database();
		$this->db->select('*');
		$this->db->from('tb_medico');
		$this->db->join('tb_pessoa', 'tb_pessoa.id_pessoa = tb_medico.fk_id_pessoa', 'inner');
	}

	public function update(){}

	public function delete(){
		try{
			$this->load->database();
			if(isset($_GET['id_pessoa'])){
		    	$this->db->where('fk_id_pessoa',$_GET['id_pessoa']);
         	   $deletou = $this->db->delete('tb_pessoa');
				return ['status' => $deletou];
			}
			return ['status' => false, 'msg' => 'A variável id_pessoa precisa ser inicializada!'];

		}catch(Exception $ex){
			return ['status' => false, 'msg' => $ex->getMessage()];
		}
	}

}
