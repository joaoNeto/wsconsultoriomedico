<?php
class Paciente_model extends CI_Model {

	public $fk_id_pessoa;
	public $cpf_paciente;
	public $rg;
	public $tipo_sanguineo;

	public function create($id_pessoa){
		try{
			$this->load->database();
			$this->fk_id_pessoa = $id_pessoa;
			$this->cpf_paciente = isset($_POST['cpf']) ? $_POST['cpf'] : '';
			$this->rg = isset($_POST['rg']) ? $_POST['rg'] : '';
			$this->tipo_sanguineo = isset($_POST['tipo_sanguineo']) ? $_POST['tipo_sanguineo'] : '';

		    return $this->db->insert('tb_paciente', $this);
		}catch(Exception $ex){
			return false;
		}
	}

	public function delete(){
		try{
			$this->load->database();
			if(isset($_GET['id_pessoa'])){
		    	$this->db->where('fk_id_pessoa',$_GET['id_pessoa']);
         	   $deletou = $this->db->delete('tb_pessoa');
				return ['status' => $deletou];
			}
			return ['status' => false, 'msg' => 'A variável id_pessoa precisa ser inicializada!'];

		}catch(Exception $ex){
			return ['status' => false, 'msg' => $ex->getMessage()];
		}
	}

}
