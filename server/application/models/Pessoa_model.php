<?php
class Pessoa_model extends CI_Model {

	// public $id_pessoa;
	public $login;
	public $senha;
	public $nome_completo;
	public $email;
	public $sexo;
	public $estado;
	public $dt_nascimento;
	public $estado_civil;
	public $telefone1;
	public $telefone2;
	public $fk_id_tipo_pessoa = 3;

	public function create(){
		$this->load->database();
		try{

		    $this->login 			 = $_POST['login'];
		    $this->senha 			 = $_POST['senha'];
		    $this->nome_completo 	 = isset($_POST['nome_completo']) ? $_POST['nome_completo'] : '';
		    $this->email 			 = isset($_POST['email']) ? $_POST['email'] : '';
		    $this->sexo 			 = isset($_POST['sexo']) ? $_POST['sexo'] : '';
		    $this->estado 			 	 = isset($_POST['estado']) ? $_POST['estado'] : '';
		    $this->dt_nascimento 	 = isset($_POST['dt_nascimento']) ? $_POST['dt_nascimento'] : '';
		    $this->estado_civil 	 = isset($_POST['estado_civil']) ? $_POST['estado_civil'] : '';
		    $this->telefone1 		 = isset($_POST['telefone1']) ? $_POST['telefone1'] : '';
		    $this->telefone2 		 = isset($_POST['telefone2']) ? $_POST['telefone2'] : '';
		    $this->fk_id_tipo_pessoa = isset($_POST['id_tipo_pessoa']) ? $_POST['id_tipo_pessoa'] : 3;

		    return $this->db->insert('tb_pessoa', $this);

		}catch(Exception $ex){
			return false;
		}
	}

	public function read($where){
		$this->load->database();
		$this->db->select('*');
		$this->db->from('tb_pessoa');
		$this->db->join('tb_atendente', 'tb_pessoa.id_pessoa = tb_atendente.fk_id_pessoa', 'left');
		$this->db->join('tb_medico', 'tb_pessoa.id_pessoa = tb_medico.fk_id_pessoa', 'left');
		$this->db->join('tb_paciente', 'tb_pessoa.id_pessoa = tb_paciente.fk_id_pessoa', 'left');

		if(isset($where['login'])){
			$this->db->where('login = ', $where['login']);
		}

		if(isset($where['senha'])){
			$this->db->where('senha = ', $where['senha']);
		}

		if(isset($where['search'])){
			$this->db->where('nome_completo like "%'.$where['search'].'%" ');
		}

		if(isset($where['id_tipo_pessoa'])){
			$this->db->where('fk_id_tipo_pessoa = ', $where['id_tipo_pessoa']);
		}

		if(isset($where['id_pessoa'])){
			$this->db->where('id_pessoa = ', $where['id_pessoa']);
		}

		// paciente não pode logar no sistema
		if(isset($where['not_show_paciente']))
			$this->db->where('fk_id_tipo_pessoa <> 3');

		return $this->db->get();
	}

	public function update(){
        try{
			$this->load->database();

			if(isset($_GET['login']))
				$this->login = $_GET['login'];

			if(isset($_GET['senha']))
				$this->senha = $_GET['senha'];
			
			if(isset($_GET['nome_completo']))
				$this->nome_comleto = $_GET['nome_completo'];

			if(isset($_GET['email']))
				$this->email = $_GET['email'];

			if(isset($_GET['sexo']))
				$this->sexo = $_GET['sexo'];

			if(isset($_GET['estado']))
				$this->estado = $_GET['estado'];

			if(isset($_GET['dt_nascimento']))
				$this->dt_nascimento = $_GET['dt_nascimento'];

			if(isset($_GET['estado_civil']))
				$this->estado_civil = $_GET['estado_civil'];
			
			if(isset($_GET['telefone1']))
				$this->telefone1 = $_GET['telefone1'];

			if(isset($_GET['telefone2']))
				$this->telefone2 = $_GET['telefone2'];

			if(isset($_GET['id_tipo_pessoa']))
				$this->id_tipo_pessoa = $_GET['id_tipo_pessoa'];


	        $result = $this->db->update('tb_pessoa', $this, array('id_pessoa' => $_GET['id_pessoa']));

	        return ['status' => $result];
        }catch(Exception $ex){
        	return ['status' => false, 'msg' => $ex->getMessage()];
        }	
	}

	public function delete(){

        try{
			$this->load->database();
			if(isset($_GET['id_pessoa'])){
		    	$this->db->where('id_pessoa',$_GET['id_pessoa']);
         	   $deletou = $this->db->delete('tb_pessoa');
				return ['status' => $deletou];
			}
			return ['status' => false, 'msg' => 'A variável id_pessoa precisa ser inicializada!'];

		}catch(Exception $ex){
			return ['status' => false, 'msg' => $ex->getMessage()];
		}
	}
	}



