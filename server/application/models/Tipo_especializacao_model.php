<?php
class Tipo_especializacao_model extends CI_Model {

	public $id_medico_especializacao;
	public $nome_especializacao;

	public function create(){

    	$this->load->database();
	try{
		$this->id_medico_especializacao = $_POST['id_especializacao'];
		$this->nome_especializacao = $_POST['nome_especializacao'];
		    
		    return $this->db->insert('tb_medico_especializacao', $this);
		}catch(Exception $ex){
			
			return false;
		}

	}

	public function read(){
		$this->load->database();
		$this->db->select('*');
		$this->db->from('tb_medico_especializacao');
		return $this->db->get();		
	}

	public function update(){}

	public function delete(){}

}
