-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 20-Nov-2018 às 21:31
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `consultorio_medico`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_atendente`
--

CREATE TABLE `tb_atendente` (
  `fk_id_pessoa` int(5) DEFAULT NULL,
  `cpf_atendente` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_atendente`
--

INSERT INTO `tb_atendente` (`fk_id_pessoa`, `cpf_atendente`) VALUES
(9, '115.439.274');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avaliacao`
--

CREATE TABLE `tb_avaliacao` (
  `sintomas` varchar(100) NOT NULL,
  `diagnostico` varchar(70) NOT NULL,
  `procedimento_imediato` varchar(100) DEFAULT NULL,
  `procedimento_posterior` varchar(100) DEFAULT NULL,
  `exame` varchar(70) DEFAULT NULL,
  `fk_id_consulta` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_consulta`
--

CREATE TABLE `tb_consulta` (
  `id_consulta` int(5) NOT NULL,
  `data` datetime NOT NULL,
  `cpf_paciente` varchar(11) NOT NULL,
  `cpf_atendente` varchar(11) NOT NULL,
  `crm` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_consulta`
--

INSERT INTO `tb_consulta` (`id_consulta`, `data`, `cpf_paciente`, `cpf_atendente`, `crm`) VALUES
(1, '1970-01-01 00:00:00', '115.439.274', '115.439.274', '123123'),
(2, '1970-01-01 00:00:00', '115.439.274', '115.439.274', '123123'),
(3, '1970-01-01 00:00:00', '115.439.274', '115.439.274', '123123');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_medico`
--

CREATE TABLE `tb_medico` (
  `fk_id_pessoa` int(5) DEFAULT NULL,
  `crm` varchar(20) NOT NULL,
  `fk_id_medico_especializacao` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_medico`
--

INSERT INTO `tb_medico` (`fk_id_pessoa`, `crm`, `fk_id_medico_especializacao`) VALUES
(15, '123123', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_medico_especializacao`
--

CREATE TABLE `tb_medico_especializacao` (
  `id_medico_especializacao` int(2) NOT NULL,
  `nome_especializacao` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_medico_especializacao`
--

INSERT INTO `tb_medico_especializacao` (`id_medico_especializacao`, `nome_especializacao`) VALUES
(1, 'pediatra'),
(2, 'dentista');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_paciente`
--

CREATE TABLE `tb_paciente` (
  `fk_id_pessoa` int(5) DEFAULT NULL,
  `cpf_paciente` varchar(20) NOT NULL,
  `rg` varchar(20) NOT NULL,
  `tipo_sanguineo` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_paciente`
--

INSERT INTO `tb_paciente` (`fk_id_pessoa`, `cpf_paciente`, `rg`, `tipo_sanguineo`) VALUES
(1, '115.439.274', '123123', ''),
(12, '115.439.274-08', '123', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_pessoa`
--

CREATE TABLE `tb_pessoa` (
  `id_pessoa` int(5) NOT NULL,
  `login` varchar(30) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `nome_completo` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `sexo` enum('m','f') DEFAULT NULL,
  `estado` varchar(2) DEFAULT NULL,
  `dt_nascimento` date DEFAULT NULL,
  `estado_civil` enum('solteiro','casado') DEFAULT 'solteiro',
  `telefone1` varchar(20) DEFAULT NULL,
  `telefone2` varchar(20) DEFAULT NULL,
  `fk_id_tipo_pessoa` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_pessoa`
--

INSERT INTO `tb_pessoa` (`id_pessoa`, `login`, `senha`, `nome_completo`, `email`, `sexo`, `estado`, `dt_nascimento`, `estado_civil`, `telefone1`, `telefone2`, `fk_id_tipo_pessoa`) VALUES
(1, 'atendente', '41d08111b64cc0a69c7b04a6d163b660', 'nome da atendente', '', NULL, NULL, NULL, 'solteiro', NULL, NULL, 1),
(2, 'medico', '41d08111b64cc0a69c7b04a6d163b660', 'carlos nascimento', '', NULL, NULL, NULL, 'solteiro', NULL, NULL, 2),
(8, '1542726111364', 'd41d8cd98f00b204e9800998ecf8427e', 'João Jose de Sousa Neto', 'eu@joaonetoprogramador.com', 'm', 'AC', '0000-00-00', '', '', '', 3),
(9, 'newAtendente', '123asd123asd', 'nome do atendente', 'atendente@gmail.com', 'm', 'AC', '0000-00-00', '', '', '', 1),
(10, 'outroAtendente', '41d08111b64cc0a69c7b04a6d163b660', 'nome do atendente', 'eu@joaonetoprogramador.com', 'm', 'AC', '0000-00-00', '', '', '', 1),
(11, 'medico2', '41d08111b64cc0a69c7b04a6d163b660', 'nome completo medico', 'eu@joaonetoprogramador.com', 'm', 'AC', '0000-00-00', '', '', '', 2),
(12, '1542741031734', 'd41d8cd98f00b204e9800998ecf8427e', 'armando ', 'armando@armando.com', 'm', 'AC', '0000-00-00', '', '', '', 3),
(13, 'medico3', '41d08111b64cc0a69c7b04a6d163b660', 'medico', 'email@email.com', 'm', 'AC', '0000-00-00', '', '', '', 2),
(14, 'medico4', '41d08111b64cc0a69c7b04a6d163b660', 'medico', 'email@email.com', 'm', 'AC', '0000-00-00', '', '', '', 2),
(15, 'medicu', '41d08111b64cc0a69c7b04a6d163b660', 'nome completo medico', '', 'm', 'AC', '0000-00-00', '', '', '', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_tipo_pessoa`
--

CREATE TABLE `tb_tipo_pessoa` (
  `id_tipo_pessoa` int(2) NOT NULL,
  `nome_tipo_pessoa` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_tipo_pessoa`
--

INSERT INTO `tb_tipo_pessoa` (`id_tipo_pessoa`, `nome_tipo_pessoa`) VALUES
(1, 'atendente'),
(2, 'medico'),
(3, 'paciente');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_atendente`
--
ALTER TABLE `tb_atendente`
  ADD PRIMARY KEY (`cpf_atendente`),
  ADD KEY `fk_id_pessoa` (`fk_id_pessoa`);

--
-- Indexes for table `tb_avaliacao`
--
ALTER TABLE `tb_avaliacao`
  ADD KEY `fk_id_consulta` (`fk_id_consulta`);

--
-- Indexes for table `tb_consulta`
--
ALTER TABLE `tb_consulta`
  ADD PRIMARY KEY (`id_consulta`),
  ADD KEY `crm` (`crm`),
  ADD KEY `cpf_atendente` (`cpf_atendente`),
  ADD KEY `cpf_paciente` (`cpf_paciente`);

--
-- Indexes for table `tb_medico`
--
ALTER TABLE `tb_medico`
  ADD PRIMARY KEY (`crm`),
  ADD KEY `fk_id_pessoa` (`fk_id_pessoa`),
  ADD KEY `fk_id_medico_especializacao` (`fk_id_medico_especializacao`);

--
-- Indexes for table `tb_medico_especializacao`
--
ALTER TABLE `tb_medico_especializacao`
  ADD PRIMARY KEY (`id_medico_especializacao`);

--
-- Indexes for table `tb_paciente`
--
ALTER TABLE `tb_paciente`
  ADD PRIMARY KEY (`cpf_paciente`),
  ADD KEY `fk_id_pessoa` (`fk_id_pessoa`);

--
-- Indexes for table `tb_pessoa`
--
ALTER TABLE `tb_pessoa`
  ADD PRIMARY KEY (`id_pessoa`),
  ADD KEY `fk_id_tipo_pessoa` (`fk_id_tipo_pessoa`);

--
-- Indexes for table `tb_tipo_pessoa`
--
ALTER TABLE `tb_tipo_pessoa`
  ADD PRIMARY KEY (`id_tipo_pessoa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_consulta`
--
ALTER TABLE `tb_consulta`
  MODIFY `id_consulta` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_medico_especializacao`
--
ALTER TABLE `tb_medico_especializacao`
  MODIFY `id_medico_especializacao` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_pessoa`
--
ALTER TABLE `tb_pessoa`
  MODIFY `id_pessoa` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_atendente`
--
ALTER TABLE `tb_atendente`
  ADD CONSTRAINT `tb_atendente_ibfk_1` FOREIGN KEY (`fk_id_pessoa`) REFERENCES `tb_pessoa` (`id_pessoa`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `tb_avaliacao`
--
ALTER TABLE `tb_avaliacao`
  ADD CONSTRAINT `tb_avaliacao_ibfk_1` FOREIGN KEY (`fk_id_consulta`) REFERENCES `tb_consulta` (`id_consulta`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `tb_consulta`
--
ALTER TABLE `tb_consulta`
  ADD CONSTRAINT `tb_consulta_ibfk_1` FOREIGN KEY (`crm`) REFERENCES `tb_medico` (`crm`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_consulta_ibfk_2` FOREIGN KEY (`cpf_atendente`) REFERENCES `tb_atendente` (`cpf_atendente`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_consulta_ibfk_3` FOREIGN KEY (`cpf_paciente`) REFERENCES `tb_paciente` (`cpf_paciente`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `tb_medico`
--
ALTER TABLE `tb_medico`
  ADD CONSTRAINT `tb_medico_ibfk_1` FOREIGN KEY (`fk_id_pessoa`) REFERENCES `tb_pessoa` (`id_pessoa`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_medico_ibfk_2` FOREIGN KEY (`fk_id_medico_especializacao`) REFERENCES `tb_medico_especializacao` (`id_medico_especializacao`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `tb_paciente`
--
ALTER TABLE `tb_paciente`
  ADD CONSTRAINT `tb_paciente_ibfk_1` FOREIGN KEY (`fk_id_pessoa`) REFERENCES `tb_pessoa` (`id_pessoa`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `tb_pessoa`
--
ALTER TABLE `tb_pessoa`
  ADD CONSTRAINT `tb_pessoa_ibfk_1` FOREIGN KEY (`fk_id_tipo_pessoa`) REFERENCES `tb_tipo_pessoa` (`id_tipo_pessoa`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
